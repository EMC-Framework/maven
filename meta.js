const { writeFileSync, readdirSync, existsSync } = require('fs');
const { stable, version } = require('./meta.json').meta;

const MAVEN_DIR = './me/deftware';
const EMC_PATHS = [ 'EMC', 'EMC-F-v2' ];
let data = {};

/**
 * @param {string} type EMC directory (EMC-F-v2, EMC)
 * @param {string} emc EMC release (stable, version e.g. 17.0.0)
 * @param {string} version Minecraft version
 * @returns Generated meta data for the version
 */
function getMeta(version, type, emc) {
    let meta = {
        type,
        version,
        emc,
        name: `me.deftware:${type}:${emc}-${version}`
    };
    if (existsSync(`${MAVEN_DIR}/EMC-Forge/${emc}-${version}`)) {
        meta['forge'] = {
            type: 'EMC-Forge',
            name: `me.deftware:EMC-Forge:${emc}-${version}`
        };
    }
    return meta;
}

class Version {
    constructor(regex, key, types, sort) {
        this.regex = regex;
        this.key = key;
        this.types = types;
        this.sort = sort;
        this.files = [];
    }

    discover() {
        for (const type of this.types) {
            const path = `${MAVEN_DIR}/${type}`;
            for (let emc of readdirSync(path)) {
                const match = emc.match(this.regex);
                if (match) {
                    const mcVersion = match[2];
                    this.files.push({ mcVersion, type });
                }
            }
        }
        this.files.sort(this.sort);
    }

    populate(map, emcVersion = version) {
        map[this.key] = {};
        for (const version of this.files) {
            map[this.key][version.mcVersion] = getMeta(version.mcVersion, version.type, emcVersion);
        }
        console.log(`${this.key}: ${this.files.map(v => v.mcVersion).join(", ")}`)
    }
}

function getSemverInt(version) {
    const data = version.split(".");
    let joined = data.join("");
    if (data.length === 2) {
        joined += "0";
    }
    return parseInt(joined);
}

function getPrefixInt(version, prefix) {
    const data = version.split("-");
    const mcVersion = getSemverInt(data[0] + data[1].substring(prefix.length));
    return mcVersion;
}

const STABLE = new Version(/^(latest)-([0-9.]+)$/, 'release', EMC_PATHS, (a, b) => {
    return getSemverInt(b.mcVersion) - getSemverInt(a.mcVersion);
});

STABLE.discover();
STABLE.populate(data, 'latest');

const SNAPSHOT = new Version(/^([0-9.]+)-([0-9]{2}w[0-9]{2}[a-z])$/, 'snapshot', EMC_PATHS, (a, b) => {
    const getName = (version) => {
        const data = version.replace(/[a-z]/g, "");
        return parseInt(data);
    };
    return getName(b.mcVersion) - getName(a.mcVersion);
});

SNAPSHOT.discover();
SNAPSHOT.populate(data);

const PRE_RELEASE = new Version(/^([0-9.]+)-([0-9.]+-pre[0-9]+)$/, 'pre_release', EMC_PATHS, (a, b) => {
    const getName = (version) => getPrefixInt(version, 'pre');
    return getName(b.mcVersion) - getName(a.mcVersion);
});

PRE_RELEASE.discover();
PRE_RELEASE.populate(data);

const RELEASE_CANDIDATE = new Version(/^([0-9.]+)-([0-9.]+-rc[0-9]+)$/, 'release_candidate', EMC_PATHS, (a, b) => {
    const getName = (version) => getPrefixInt(version, "rc");
    return getName(b.mcVersion) - getName(a.mcVersion);
});

RELEASE_CANDIDATE.discover();
RELEASE_CANDIDATE.populate(data);

let meta = {
    stable,
    version
};
for (let type of Object.keys(data)) {
    meta[type] = Object.values(data[type])[0];
}

data.meta = meta;
data.date = new Date().toISOString();

writeFileSync('./meta.json', JSON.stringify(data, null, 4));
